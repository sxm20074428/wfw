package com.sise.lh.controller;
import com.sise.lh.model.User;
import com.sise.lh.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * 作者：idea
 * 日期：2018/5/2
 * 描述：控制器
 */
@RestController
public class UserController {
    @Autowired
    public UserService userService;

    @RequestMapping(value = "/v0/user/find/one")
    public User findOne(){
        return userService.findOne();
    }
}
