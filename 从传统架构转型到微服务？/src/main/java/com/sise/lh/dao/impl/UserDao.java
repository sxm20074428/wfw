package com.sise.lh.dao.impl;

import com.sise.lh.dao.IUserDao;
import com.sise.lh.model.User;
import org.springframework.stereotype.Repository;

/**
 * 作者：idea
 * 日期：2018/5/2
 * 描述：用户dao
 */
@Repository
public class UserDao implements IUserDao{

    public User findOne(){
        return new User(1,"idea","password");
    }

}




