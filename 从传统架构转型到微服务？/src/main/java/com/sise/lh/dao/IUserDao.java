package com.sise.lh.dao;
import com.sise.lh.model.User;
import org.springframework.stereotype.Repository;

/**
 * 作者：idea
 * 日期：2018/5/2
 * 描述：user接口
 */

public interface IUserDao {
    public User findOne();
}
