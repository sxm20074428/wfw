package com.sise.lh.controller;
import com.sise.lh.model.User;
import com.sise.lh.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

/**
 * 作者：idea
 * 日期：2018/5/2
 * 描述：控制器
 */
@Controller
public class UserController {
    @Autowired
    public UserService userService;

    @RequestMapping(value = "/v0/user/login")
    public String login(String account,String password){
        if(userService.login(account,password)){
            return "/sys/sys_index";
        }else{
            return "/login_fail";
        }
    }
}
