package com.sise.lh.model;

import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;

/**
 * 作者：idea
 * 日期：2018/5/2
 * 描述：用户对象
 */
@Table(name = "user")
@Entity
public class User {

    @Id
    @GeneratedValue
    @Column(name = "id")
    public int id;
    @Column(name = "username")
    public String username;
    @Column(name = "password")
    public String password;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    @Override
    public String toString() {
        return "User{" +
                "id=" + id +
                ", username='" + username + '\'' +
                ", password='" + password + '\'' +
                '}';
    }

    public User() {
    }

    public User(int id, String username, String password) {
        this.id = id;
        this.username = username;
        this.password = password;
    }
}
