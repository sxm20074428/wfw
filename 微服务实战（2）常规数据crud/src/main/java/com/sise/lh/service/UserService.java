package com.sise.lh.service;

import com.sise.lh.dao.IUserDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * 作者：idea
 * 日期：2018/5/2
 * 描述：
 */
@Service
public class UserService {

    @Autowired
    public IUserDao userDao;

    public boolean login(String username,String password){
        return (userDao.findByUsernameAndPassword(username, password)!=null)?true:false;
    }

}
