package com.sise.lh.dao;
import com.sise.lh.model.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

/**
 * 作者：idea
 * 日期：2018/5/2
 * 描述：user dao接口
 */
public interface IUserDao  extends JpaRepository<User,Integer>{
        User findByUsernameAndPassword(String username,String password);
        @Query("select * from User where ")
        User findUserByUserName(String username);
}
