package com.sise.lh.service;

import com.sise.lh.dao.AccountDao;
import com.sise.lh.model.Account;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

/**
 * @author idea
 * @data 2019/7/6
 */
@Service
public class AccountService {

    @Autowired
    private AccountDao accountDao;
    @Autowired
    private UserAccountService userAccountService;

    /**
     * 外层定义事务， userAccountService.saveOne单独定义事务
     *
     * @param accountId
     * @param money
     */
    @Transactional(propagation = Propagation.REQUIRED)
    public void saveOne(Integer accountId, Double money) {
        accountDao.insert(new Account(accountId, money));
        userAccountService.saveOne("idea", 1001);
    }
}
