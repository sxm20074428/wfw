package com.sise.lh.dao;

import com.sise.lh.model.UserAccount;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;

/**
 * @author idea
 * @data 2019/7/6
 */
@Mapper
public interface UserAccountDao {

    @Insert("insert into user_account (username,accountId) values (#{username},#{accountId}) ")
    void insert(UserAccount userAccount);
}
