package com.sise.lh.service;

import com.sise.lh.dao.UserAccountDao;
import com.sise.lh.model.UserAccount;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

/**
 * @author idea
 * @data 2019/7/6
 */
@Service
public class UserAccountService {

    @Autowired
    private UserAccountDao userAccountDao;


    /**
     *  PROPAGATION_NOT_SUPPORTED 可以用于发送提示消息，站内信、短信、邮件提示等。不属于并且不应当影响主体业务逻辑，即使发送失败也不应该对主体业务逻辑回滚。
     *  但是本身不会因为出了异常而进行事务回滚
     *
     *  PROPAGATION_REQUIRES_NEW 总是新启一个事物，这个传播机制适用于不受父方法事物影响的操作，比如某些业务场景下需要记录业务日志，用于异步反查，
     *  那么不管主体业务逻辑是否完成，日志都需要记录下来，不能因为主体业务逻辑报错而丢失日志；但是本身是一个单独的事物，会受到回滚的影响。
     *  但是父类回滚的时候也要回滚子类就不能考虑这种了
     *
     *  MANDATORY 强制使用当期的事物，如果当前的父类方法没有事务，那么在处理数据的时候就会抛出异常 mandatory(强制的)
     *
     *  NEVER 当前如果存在事务则抛出异常 Existing transaction found for transaction marked with propagation 'never'
     *
     *  NESTED 如果子事务出现了异常，不希望影响父类事务，那么需要通过try catch来控制，
     *  如果说父类事务出现了异常，回滚之后必须回滚子事务，那么就需要用Nested级别。
     *
     * @param username
     * @param accountId
     */
    @Transactional(propagation = Propagation.NESTED)
    public void saveOne(String username,Integer accountId){
        userAccountDao.insert(new UserAccount(username,accountId));
        int i=1/0;
    }
}
