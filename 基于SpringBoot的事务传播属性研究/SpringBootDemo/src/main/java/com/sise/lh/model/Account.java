package com.sise.lh.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author idea
 * @data 2019/7/6
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Account {

    private Integer accountId;

    private Double money;
}
