package com.sise.lh.dao;

import com.sise.lh.model.Account;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;

/**
 * @author idea
 * @data 2019/7/6
 */
@Mapper
public interface AccountDao {

    @Insert("insert into account (accountId,money) values (#{accountId},#{money}) ")
    void insert(Account account);
}
