package com.sise.lh.controller;

import com.sise.lh.service.AccountService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author idea
 * @data 2019/7/6
 */
@RequestMapping(value = "/test")
@RestController
public class TestController {


    @Autowired
    private AccountService accountService;

    @RequestMapping(value = "/t")
    public void test() {
        accountService.saveOne(1001, (double) 99);
        System.out.println("执行完毕");
    }
}
