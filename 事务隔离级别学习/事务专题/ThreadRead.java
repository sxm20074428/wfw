package 事务专题;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

/**
 * @author idea
 * @data 2019/7/1
 */
public class ThreadRead implements Runnable {

    @Override
    public void run() {
        select();
    }

    public void select() {
        try {
            Thread.sleep(500);
            String sql = "select * from money";
            Connection conn = JdbcUtil.getConnection();
            conn.setTransactionIsolation(Connection.TRANSACTION_READ_COMMITTED);
            PreparedStatement ps = conn.prepareStatement(sql);
            ResultSet rs = ps.executeQuery();
            System.out.println("执行读取数据操作----");
            List list = JdbcUtil.convertList(rs);
            for (Object o : list) {
                System.out.println(o);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }


    public static void main(String[] args) {
        Thread tr = new Thread(new ThreadRead());
        tr.start();
    }

}
