package 事务专题;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

/**
 * @author idea
 * @data 2019/7/1
 */
public class ThreadWrite implements Runnable {

    @Override
    public void run() {
        write();
    }

    public void write() {
        try (Connection conn = JdbcUtil.getConnection();) {
            conn.setAutoCommit(false);
            PreparedStatement ps = conn.prepareStatement("INSERT INTO `dev`.`money` (`id`, `money`) VALUES ('3', '350');");
            ps.executeUpdate();
            System.out.println("执行写取数据操作----");
            Thread.sleep(10000);
            ps.close();
        } catch (SQLException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public static void main(String[] args) {
        Thread tw=new Thread(new ThreadWrite());
        tw.start();
    }

}
