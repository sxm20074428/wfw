package com.sise.spring_task;

import lombok.extern.slf4j.Slf4j;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

/**
 * 作者：idea
 * 日期：2018/5/7
 * 描述：定时任务
 */
@Slf4j
@Component
public class ScheduledService {

    @Scheduled(cron = "*/5 * * * * ?")
    public void scheduled(){
        System.out.println("(每隔五秒执行一次)定时任务"+System.currentTimeMillis());
    }

    @Scheduled(fixedRate = 5000)
    public void scheduled1() {
        System.out.println("(执行间隔5秒)定时任务"+System.currentTimeMillis());
    }

    @Scheduled(fixedDelay = 5000)
    public void scheduled2() {
        System.out.println("(每个方法之间都差5秒)定时任务"+System.currentTimeMillis());
    }

}
