package com.sise.quartz;

import org.quartz.*;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * 作者：idea
 * 日期：2018/5/7
 * 描述：定时任务配置类
 */
@Configuration
public class QuartzConfig {
    @Bean
    //执行具体的任务
    public JobDetail quartDetail(){
        return JobBuilder.newJob(TestQuartz.class).withIdentity("testQuart").storeDurably().build();
    }

    @Bean
    //指定出发规则（每隔10毫秒）
    public Trigger quartTrigger(){
        SimpleScheduleBuilder scheduleBuilder=SimpleScheduleBuilder.simpleSchedule()
                .withIntervalInMilliseconds(10)
                .repeatForever();
        return TriggerBuilder.newTrigger().forJob(quartDetail())
                .withIdentity("testQuart")
                .withSchedule(scheduleBuilder)
                .build();
    }

}
