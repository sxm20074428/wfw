package com.sise.quartz;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;

/**
 * 作者：idea
 * 日期：2018/5/7
 * 描述：测试入口
 */
@SpringBootApplication
public class Test {
    public static void main(String[] args) {
        SpringApplication.run(Test.class,args);
    }
}
