package com.sise.quartz;

import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.springframework.scheduling.quartz.QuartzJobBean;
import java.util.Date;

/**
 * 作者：idea
 * 日期：2018/5/7
 * 描述：
 */
public class TestQuartz extends QuartzJobBean{
    @Override
    protected void executeInternal(JobExecutionContext jobExecutionContext) throws JobExecutionException {
        System.out.println("定时任务"+new Date());
    }



}
