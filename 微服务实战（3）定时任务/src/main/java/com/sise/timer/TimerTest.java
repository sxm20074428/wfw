package com.sise.timer;


import java.util.Timer;
import java.util.TimerTask;

/**
 * 作者：idea
 * 日期：2018/5/7
 * 描述：Timer执行定时任务
 */
public class TimerTest {
    public static void main(String[] args) {
        TimerTask timerTest=new TimerTask(){
            @Override
            public void run(){
                System.out.println("定时器执行任务！");
            }
        };
        Timer timer=new Timer();
        //每个一秒执行一次，执行10次
        timer.schedule(timerTest,10,1000);
    }


}
