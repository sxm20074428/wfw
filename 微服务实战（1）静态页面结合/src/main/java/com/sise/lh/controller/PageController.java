package com.sise.lh.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * 作者：idea
 * 日期：2018/5/3
 * 描述：页面跳转控制器
 */
@Controller
public class PageController {

    @RequestMapping(value = "/v0/page/login")
    public String loginPage(){
        return "/login";
    }

}
