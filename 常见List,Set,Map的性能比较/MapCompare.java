import org.junit.Before;
import org.junit.Test;
import java.util.Set;
import java.util.HashMap;
import java.util.Iterator;
import java.util.TreeMap;

/**
 * 作者：idea
 * 日期：2018/5/18
 * 描述：
 */
//结论： HashMap：适用于Map中插入、删除和定位元素。
//Treemap：适用于按自然顺序或自定义顺序遍历键(key)。
//    treemap总体的性能不如hashMap
public class MapCompare {
    public HashMap hashMap;
    public TreeMap treeMap;
    @Before
    public void setUp(){
        hashMap=new HashMap();
        treeMap=new TreeMap();
    }

    @Test
    public void testAddContentInHashMap(){
        long begin=System.currentTimeMillis();
        for(int i=0;i<100000;i++){
            hashMap.put(i,i);
        }
        long end=System.currentTimeMillis();
        System.out.println(hashMap.getClass()+"插入数据消耗时间为"+(end-begin)+"ms");
    }

    @Test
    public void testAddContentInTreeMap(){
        long begin=System.currentTimeMillis();
        for(int i=0;i<100000;i++){
            treeMap.put(i,i);
        }
        long end=System.currentTimeMillis();
        System.out.println(treeMap.getClass()+"插入数据消耗时间为"+(end-begin)+"ms");
    }

    @Test
    public void testRemoveFromTreeMap(){
        for(int i=0;i<100000;i++){
            treeMap.put(i,i);
        }
        long begin=System.currentTimeMillis();
        Set keySet=treeMap.keySet();
        Iterator iterator=keySet.iterator();
        while(iterator.hasNext()){
            iterator.next();
            iterator.remove();
        }
        long end=System.currentTimeMillis();
        System.out.println(treeMap.getClass()+"清除数据消耗时间为"+(end-begin)+"ms");
    }

    @Test
    public void testRemoveFromHashMap(){
        for(int i=0;i<100000;i++){
            hashMap.put(i,i);
        }
        long begin=System.currentTimeMillis();
        Set keySet=hashMap.keySet();
        Iterator iterator=keySet.iterator();
        while(iterator.hasNext()){
            iterator.next();
            iterator.remove();
        }
        long end=System.currentTimeMillis();
        System.out.println(hashMap.getClass()+"清除数据消耗时间为"+(end-begin)+"ms");
    }
}
