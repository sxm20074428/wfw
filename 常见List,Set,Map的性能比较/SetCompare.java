import org.junit.Before;
import org.junit.Test;

import javax.validation.constraints.Size;
import java.util.*;

/**
 * 作者：idea
 * 日期：2018/5/18
 * 描述：
 */
//结论，对于hashset和treeset而言，两者对于数据的处理原理不同，
//   TreeSet 是二差树（红黑树的树据结构）实现的,Treeset中的数据是自动排好序的，不允许放入null值
//    HashSet 是哈希表实现的,HashSet中的数据是无序的，适合用于存储一些需要快速查找的数据
//    TreeSet适合用于进行数据排序
public class SetCompare {

    public Set hashSet;
    public Set treeSet;
    public static int SIZE=100000;
    @Before
    public void setUp(){
        hashSet=new HashSet();
        treeSet=new TreeSet();
    }

    @Test
    public void addInHashSet(){
        long begin=System.currentTimeMillis();
        for(int i=0;i<SIZE;i++){
            hashSet.add(i);
        }
        long end=System.currentTimeMillis();
        System.out.println(hashSet.getClass()+" 添加"+SIZE+"数据消耗时间"+(end-begin)+"ms");
    }


    @Test
    public void testFindFromHashSet(){

        for(int i=0;i<SIZE;i++){
            hashSet.add(i);
        }
        long begin=System.currentTimeMillis();
        Iterator iterator=hashSet.iterator();
        int item;
        while(iterator.hasNext()){
            item= (int) iterator.next();
            if (item== (SIZE/2)){
                break;
            }
        }
        long end=System.currentTimeMillis();
        System.out.println(hashSet.getClass()+" 查找数据消耗时间"+(end-begin)+"ms");
    }

    @Test
    public void testFindFromTreeSet(){

        for(int i=0;i<SIZE;i++){
            treeSet.add(i);
        }
        long begin=System.currentTimeMillis();
        Iterator iterator=treeSet.iterator();
        int item;
        while(iterator.hasNext()){
            item= (int) iterator.next();
            if (item== (SIZE/2)){
                break;
            }
        }
        long end=System.currentTimeMillis();
        System.out.println(treeSet.getClass()+" 查找数据消耗时间"+(end-begin)+"ms");
    }

    @Test
    public void addInTreeSet(){
        long begin=System.currentTimeMillis();
        for(int i=0;i<SIZE;i++){
            treeSet.add(i);
        }
        long end=System.currentTimeMillis();
        System.out.println(treeSet.getClass()+" 添加"+SIZE+"数据消耗时间"+(end-begin)+"ms");
    }

    @Test
    public void testRemoveFromTreeSet(){
        for(int i=0;i<SIZE;i++){
            treeSet.add(i);
        }
        long begin=System.currentTimeMillis();
        Iterator iterator=treeSet.iterator();
        int item;
        while(iterator.hasNext()){
            iterator.next();
           iterator.remove();
        }
        long end=System.currentTimeMillis();
        System.out.println(treeSet.getClass()+" 删除数据消耗时间"+(end-begin)+"ms");
    }

    @Test
    public void testRemoveFromHashSet(){
        for(int i=0;i<SIZE;i++){
            hashSet.add(i);
        }
        long begin=System.currentTimeMillis();
        Iterator iterator=hashSet.iterator();
        int item;
        while(iterator.hasNext()){
            iterator.next();
            iterator.remove();
        }
        long end=System.currentTimeMillis();
        System.out.println(treeSet.getClass()+" 删除数据消耗时间"+(end-begin)+"ms");
    }
}
