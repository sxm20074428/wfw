import org.junit.Before;
import org.junit.Test;

import java.util.*;

/**
 * 作者：idea
 * 日期：2018/5/18
 * 描述：list类型集合比较
 */

public class ListCompare {

    public List arrayList;
    public List linkList;
    public Vector vector;
    public static int SIZE=100000;
    @Before
    public void setUp() {
        arrayList = new ArrayList();
        linkList = new LinkedList();
        vector=new Vector();
    }


    private void addContentInList(List list){
        long begin=System.currentTimeMillis();
        for(int i=0;i<SIZE;i++){
            list.add(i);
        }
        long end=System.currentTimeMillis();
        System.out.println(list.getClass()+" 插入"+SIZE+"数据消耗时间"+(end-begin)+"ms");
    }

    private void removeContentFormList(List list){
        long begin=System.currentTimeMillis();
        Iterator iterator = list.iterator();
        while (iterator.hasNext()){
            Integer next = (Integer) iterator.next();
            iterator.remove();
        }
        long end=System.currentTimeMillis();
        System.out.println(list.getClass()+" 插移除"+SIZE+"数据消耗时间"+(end-begin)+"ms");
    }

    //测试10万条数据的搜索速度
    //经过测试得出，数据量大的情况下，选择arraylist对于数据存储会比较适合，假若需要对数据进行移除较多，使用linkedlist会比较适合
    @Test
    public void testArrayListAddAndRemove(){
        addContentInList(arrayList);
        removeContentFormList(arrayList);
    }

    @Test
    public void testLinkListAddAndRemove(){
        addContentInList(linkList);
        removeContentFormList(linkList);
    }

    @Test
    public void testArrayListFindOne(){
        addContentInList(arrayList);
        long begin=System.currentTimeMillis();
        arrayList.get(55555);
        long end=System.currentTimeMillis();
        System.out.println("消耗时间"+(end-begin)+"ms");
    }

    @Test
    public void testLinkListFindOne(){
        addContentInList(linkList);
        long begin=System.currentTimeMillis();
        linkList.get(55555);
        long end=System.currentTimeMillis();
        System.out.println("消耗时间"+(end-begin)+"ms");
    }


    @Test
    public void testVector(){
        long begin=System.currentTimeMillis();
        for(int i=0;i<SIZE;i++){
            vector.add(i);
        }
        long end=System.currentTimeMillis();
        System.out.println(vector.getClass()+"插入100000数据消耗时间"+(end-begin)+"ms");
         begin=System.currentTimeMillis();
         Iterator iterator= vector.iterator();
            while (iterator.hasNext()){
            Integer next = (Integer) iterator.next();
            iterator.remove();
            }
         end=System.currentTimeMillis();
        System.out.println(vector.getClass()+"删除100000数据消耗时间"+(end-begin)+"ms");

    }


}
